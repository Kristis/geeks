/*Chart 1*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart1);
function drawChart1() {
  var data = google.visualization.arrayToDataTable([
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)'],
    ['2014', 534,      400],
    ['2015',  673,      550],
    ['2016',  734,       700],
    ['2017',  733,      565]
  ]);

  var options = {
    title: 'Rinkuškių alaus pardavimai',
    hAxis: {title: 'Metai', titleTextStyle: {color: '#333'}}
 };

var chart = new google.visualization.ColumnChart(document.getElementById('chart_div1'));
  chart.draw(data, options);
}
// /*Chart 1 end

/*Chart 2*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart2);
function drawChart2() {
  var data = google.visualization.arrayToDataTable([
    ['Metai', 'Mln. EUR', ''],
    ['2013',  6.57,      0],
    ['2014',  4.19,      0],
    ['2015',  12.33,       0],
    ['2016',  11.84,      0]
  ]);

  var options = {
    title: 'Sakiškių alaus pelnas',
    hAxis: {title: 'Metai',  titleTextStyle: {color: '#333'}},
    vAxis: {minValue: 0}
  };

  var chart = new google.visualization.AreaChart(document.getElementById('chart_div2'));
  chart.draw(data, options);
}
 /*Chart 2 end*/

 /*Chart 3*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart3);
function drawChart3() {
  
  // BEGIN BAR CHART
  
  // create zero data so the bars will 'grow'
  var barZeroData = google.visualization.arrayToDataTable([
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)'],
    ['2011',  0,      0],
    ['2012',  0,      0],
    ['2013',  0,      0],
    ['2013',  0,      0],
    ['2014',  0,      0],
    ['2015',  0,      0],
    ['2016',  0,      0]
  ]);
  
  // actual bar chart data
  var barData = google.visualization.arrayToDataTable([
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)'],
    ['2011',  1050,      600],
    ['2012',  1370,      910],
    ['2013',  700,       400],
    ['2014',  1030,      540],
    ['2015',  500,      480],
    ['2016',  1170,      960],
    ['2017',  1240,       700]
  ]);
  // set bar chart options
  var barOptions = {
    focusTarget: 'category',
    backgroundColor: 'transparent',
    colors: ['cornflowerblue', 'tomato'],
    fontName: 'Open Sans',
    chartArea: {
      left: 50,
      top: 10,
      width: '100%',
      height: '70%'
    },
    bar: {
      groupWidth: '80%'
    },
    hAxis: {
      textStyle: {
        fontSize: 11
      }
    },
    vAxis: {
      minValue: 0,
      maxValue: 1500,
      baselineColor: '#DDD',
      gridlines: {
        color: '#DDD',
        count: 4
      },
      textStyle: {
        fontSize: 11
      }
    },
    legend: {
      position: 'bottom',
      textStyle: {
        fontSize: 12
      }
    },
    animation: {
      duration: 1200,
      easing: 'out',
      startup: true
    }
  };
  // draw bar chart twice so it animates
  var barChart = new google.visualization.ColumnChart(document.getElementById('bar-chart'));
  //barChart.draw(barZeroData, barOptions);
  barChart.draw(barData, barOptions);
}
  /*Chart 3 Ends*/

  /*Chart 4*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart4);
function drawChart4() {
  
  // BEGIN PIE CHART
  
  // pie chart data
  var pieData = google.visualization.arrayToDataTable([
    ['Eksporto šalis', 'Eksportuojama litrų alaus per metus'],
    ['SUOMIJA',  5498],
    ['RUSIJA',  2365],
    ['LENKIJA',  4581],
    ['LATVIJA',  3257],
    ['VOKIETIJA',  2163]
  ]);
  // pie chart options
  var pieOptions = {
    backgroundColor: 'transparent',
    pieHole: 0.4,
    colors: [ "cornflowerblue", 
              "olivedrab", 
              "orange", 
              "tomato", 
              "crimson", 
              "purple", 
              "turquoise", 
              "forestgreen", 
              "navy", 
              "gray"],
    pieSliceText: 'value',
    tooltip: {
      text: 'percentage'
    },
    fontName: 'Open Sans',
    chartArea: {
      width: '85%',
      height: '85%'
    },
    legend: {
      textStyle: {
        fontSize: 13
      }
    }
  };
  // draw pie chart
  var pieChart = new google.visualization.PieChart(document.getElementById('pie-chart'));
  pieChart.draw(pieData, pieOptions);
}
/*Chart 4 Ends*/

/*Chart 5*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart5);
function drawChart5() {
  
  // BEGIN LINE GRAPH
  
  function randomNumber(base, step) {
    return Math.floor((Math.random()*step)+base);
  }
  function createData(year, start1, start2, step, offset) {
    var ar = [];
    for (var i = 0; i < 12; i++) {
      ar.push([new Date(year, i), randomNumber(start1, step)+offset, randomNumber(start2, step)+offset]);
    }
    return ar;
  }
  var randomLineData = [
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)']
  ];
  for (var x = 0; x < 7; x++) {
    var newYear = createData(2011+x, 10000, 5000, 4000, 800*Math.pow(x,2));
    for (var n = 0; n < 12; n++) {
      randomLineData.push(newYear.shift());
    }
  }
  var lineData = google.visualization.arrayToDataTable(randomLineData);
  
  
  var animLineData = [
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)']
  ];
  for (var x = 0; x < 7; x++) {
    var zeroYear = createData(2011+x, 0, 0, 0, 0);
    for (var n = 0; n < 12; n++) {
      animLineData.push(zeroYear.shift());
    }
  }
  var zeroLineData = google.visualization.arrayToDataTable(animLineData);
  

  var lineOptions = {
    backgroundColor: 'transparent',
    colors: ['cornflowerblue', 'tomato'],
    fontName: 'Open Sans',
    focusTarget: 'category',
    chartArea: {
      left: 50,
      top: 10,
      width: '100%',
      height: '70%'
    },
    hAxis: {
      //showTextEvery: 12,
      textStyle: {
        fontSize: 11
      },
      baselineColor: 'transparent',
      gridlines: {
        color: 'transparent'
      }
    },
    vAxis: {
      minValue: 0,
      maxValue: 50000,
      baselineColor: '#DDD',
      gridlines: {
        color: '#DDD',
        count: 4
      },
      textStyle: {
        fontSize: 11
      }
    },
    legend: {
      position: 'bottom',
      textStyle: {
        fontSize: 12
      }
    },
    animation: {
      duration: 1200,
      easing: 'out',
      startup: true
    }
  };

  var lineChart = new google.visualization.LineChart(document.getElementById('line-chart'));
  //lineChart.draw(zeroLineData, lineOptions);
  lineChart.draw(lineData, lineOptions);  
}
/*Chart 5 Ends*/

/*Chart 6*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart6);
function drawChart6() {
  var data = google.visualization.arrayToDataTable([
    ['Metai', 'Mln. EUR', ''],
    ['2013',  12.86,      0],
    ['2014',  24.47,      0],
    ['2015',  19.89,       0],
    ['2016',  25.57,      0]
  ]);

  var options = {
    title: 'Įmonės pelnas',
    hAxis: {title: 'Metai',  titleTextStyle: {color: '#333'}},
    vAxis: {minValue: 0}
  };

  var chart = new google.visualization.AreaChart(document.getElementById('chart_div3'));
  chart.draw(data, options);
}
/*Chart 6 Ends*/

/*Chart 7*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart7);
function drawChart7() {
  
  // BEGIN LINE GRAPH
  
  function randomNumber(base, step) {
    return Math.floor((Math.random()*step)+base);
  }
  function createData(year, start1, start2, step, offset) {
    var ar = [];
    for (var i = 0; i < 12; i++) {
      ar.push([new Date(year, i), randomNumber(start1, step)+offset, randomNumber(start2, step)+offset]);
    }
    return ar;
  }
  var randomLineData = [
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)']
  ];
  for (var x = 0; x < 7; x++) {
    var newYear = createData(2011+x, 10000, 5000, 4000, 800*Math.pow(x,2));
    for (var n = 0; n < 12; n++) {
      randomLineData.push(newYear.shift());
    }
  }
  var lineData = google.visualization.arrayToDataTable(randomLineData);
  
  
  var animLineData = [
    ['Metai', 'Parduota šviesaus alaus litrais (tūkst.)', 'Parduota tamsaus alaus litrais (tūkst.)']
  ];
  for (var x = 0; x < 7; x++) {
    var zeroYear = createData(2011+x, 0, 0, 0, 0);
    for (var n = 0; n < 12; n++) {
      animLineData.push(zeroYear.shift());
    }
  }
  var zeroLineData = google.visualization.arrayToDataTable(animLineData);
  

  var lineOptions = {
    backgroundColor: 'transparent',
    colors: ['cornflowerblue', 'tomato'],
    fontName: 'Open Sans',
    focusTarget: 'category',
    chartArea: {
      left: 50,
      top: 10,
      width: '100%',
      height: '70%'
    },
    hAxis: {
      //showTextEvery: 12,
      textStyle: {
        fontSize: 11
      },
      baselineColor: 'transparent',
      gridlines: {
        color: 'transparent'
      }
    },
    vAxis: {
      minValue: 0,
      maxValue: 50000,
      baselineColor: '#DDD',
      gridlines: {
        color: '#DDD',
        count: 4
      },
      textStyle: {
        fontSize: 11
      }
    },
    legend: {
      position: 'bottom',
      textStyle: {
        fontSize: 12
      }
    },
    animation: {
      duration: 1200,
      easing: 'out',
      startup: true
    }
  };

  var lineChart = new google.visualization.LineChart(document.getElementById('line-chart1'));
  //lineChart.draw(zeroLineData, lineOptions);
  lineChart.draw(lineData, lineOptions);  
}
/*Chart 7 Ends*/

/*Chart 8*/
google.load("visualization", "1", {packages:["corechart"]});
google.setOnLoadCallback(drawChart8);
function drawChart8() {
  
  // BEGIN PIE CHART
  
  // pie chart data
  var pieData = google.visualization.arrayToDataTable([
    ['Eksporto šalis', 'Eksportuojama litrų alaus per metus'],
    ['LENKIJA',  8457],
    ['SUOMIJA',  6563],
    ['ESTIJA',  3547],
    ['JAV',  1589],
    ['DANIJA',  2150]
  ]);
  // pie chart options
  var pieOptions = {
    backgroundColor: 'transparent',
    pieHole: 0.4,
    colors: [ "cornflowerblue", 
              "olivedrab", 
              "orange", 
              "tomato", 
              "crimson", 
              "purple", 
              "turquoise", 
              "forestgreen", 
              "navy", 
              "gray"],
    pieSliceText: 'value',
    tooltip: {
      text: 'percentage'
    },
    fontName: 'Open Sans',
    chartArea: {
      width: '85%',
      height: '85%'
    },
    legend: {
      textStyle: {
        fontSize: 13
      }
    }
  };
  // draw pie chart
  var pieChart = new google.visualization.PieChart(document.getElementById('pie-chart1'));
  pieChart.draw(pieData, pieOptions);
}
/*Chart 8 End*/

/*Draws charts*/
$(window).resize(function(){
  drawChart1();
  drawChart2();
  drawChart3();
  drawChart4();
  drawChart5();
  drawChart6();
  drawChart7();
  drawChart8();
})

