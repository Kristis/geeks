/*Mobile collapse*/
$( document ).ready(function(){ 
	$(".button-collapse").sideNav();
})

/*Dropdown btn*/
$(document).ready(function(){
	$(".dropdown-button").dropdown();        
})

/*Slider*/
$(document).ready(function(){
    $('.slider').slider();
});

/*Tabs (AboutUs)*/
$(document).ready(function(){
    $('ul.tabs').tabs();
  });

/*Age Verification*/
$(document).ready(function(){ 
    $.ageCheck({minAge: 18});        
});

/*Animate when seen*/
$(document ).ready(function(){ 
  new WOW().init();
})