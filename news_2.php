<?php
include "dbcon.php";
?>

<!DOCTYPE html>
<html>
<head>
	<!-- LT kalba -->
	<meta charset="utf-8">

	<!-- Let browser know website is optimized for mobile -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>

	<!-- Google Icon Font -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Materialize css -->
	<link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>

	<!-- Social Media Icons css -->
    <link rel='stylesheet prefetch' href='http:////netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css'>
	<link rel="stylesheet" href="css/style1.css">
	<link rel="stylesheet" type="text/css" href="css/style2.css">

	<!-- Main css -->
    <link rel="stylesheet" type="text/css" href="main.css">

    <!-- Mobile css -->
	<link rel="stylesheet" type="text/css" href="css/mobile.css">

	<!-- Animation css -->
    <link rel="stylesheet" href="css/animate.css">

    <!-- Animated beer logo css-->
    <link rel="stylesheet" type="text/css" href="css/animated_beer_logo.css">

	<!-- Age Verification css -->
	<link rel="stylesheet" href="css/agecheck.css"/>

	<!-- Jquery js -->
	<script src="http://code.jquery.com/jquery-2.1.3.js"></script>

	<!-- Age Verification js -->
	<script src="js/jquery.agecheck.js"></script>

	<!-- Materialize js -->
    <script type="text/javascript" src="js/materialize.min.js"></script>

	<!-- Main js -->
    <script type="text/javascript" src="main.js"></script>

	<!-- Animation js --> 
    <script src="js/wow.min.js"></script>
 
	<!-- Browser Tab Logo png-->
    <link rel="icon" href="images/logo.png">

    <!-- Title -->
    <title>Lietuvos Alus</title>
</head>

<body class="light_color">	
	<!-- Navigacija-->
	<div>
		<!-- Dropdown1 Structure -->
		<ul id="dropdown1" class="dropdown-content">
			<li><a href="about.html">Apie mus</a></li>
			<li class="divider"></li>
			<li><a href="about_ausra.html">Aušra</a></li>
			<li><a href="about_katerina.html">Katerina</a></li>		
			<li><a href="about_saulius.html">Saulius</a></li>
			<li><a href="about_tauras.html">Tauras</a></li>
		</ul>

		<!-- Dropdown2 Structure -->
		<ul id="dropdown2" class="dropdown-content">
			<li><a href="about.html">Apie mus</a></li>
			<li class="divider"></li>
			<li><a href="about_ausra.html">Aušra</a></li>
			<li><a href="about_katerina.html">Katerina</a></li>		
			<li><a href="about_saulius.html">Saulius</a></li>
			<li><a href="about_tauras.html">Tauras</a></li>
		</ul>
		<nav>
		    <div class="dark_color nav-wrapper z-depth-5">

			    <!-- Animated beer logo -->	
			    <div>
				    <a href="index.html" class="left brand-logo hide-on-med-and-down">
			    		<div id="glass">
							<div id="beer"></div>
						</div>
						<div id="poignet"></div>
						<div id="mousse_1"></div>
						<div id="mousse_2"></div>
						<div id="mousse_3"></div>
						<div id="mousse_4"></div>
						<div id="mousse_5"></div>
						<div id="mousse_volante"></div>
						<div id="mousse_interieur"></div>
						<div id="mousse_interieur_2"></div>
						<div id="mousse_interieur_3"></div>
				    </a>
			    </div>
			    <p class="wow fadeInDown beer_text">LIETUVOS ALUS</p>

			    <!-- Dropdown menu -->
			    <a href="index.html" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
			    <ul class="right hide-on-med-and-down">
			        <li><a href="index.html">Pradžia</a></li>
			        <li><a href="news.html">Naujienos</a></li>	

			        <!-- Dropdown Trigger -->
		      		<li><a class="dropdown-button" href="#!" data-activates="dropdown1">Apie mus<i class="material-icons right">arrow_drop_down</i></a></li>
			    </ul>

			    <!-- Dropdowmn mobile menu -->
			    <ul class="side-nav" id="mobile-demo">
			        <li><a href="index.html">Pradžia</a></li>
			        <li><a href="news.html">Naujienos</a></li>	

			        <!-- Dropdown Trigger -->
		      		<li><a class="dropdown-button" href="about.html" data-activates="dropdown2">Apie mus<i class="material-icons right">arrow_drop_down</i></a></li>
			    </ul>
		    </div>
	 	</nav>
	</div>

	<!--Breadcrumbs-->
	<div class="light_color bc">	
	    <div class="col s12 bc_col">
	        <a href="index.html" class="breadcrumb bc_text">Pradžia</a>
	        <a href="news.html" class="breadcrumb bc_text">Naujienos</a>
	        <a href="news_2.php?id=2" class="breadcrumb bc_text">2</a>
	    </div>
	</div> 
</div>

<!-- Main -->
<div class="row light_color">
	<div class="col s12 m12 l12 z-depth-4" style="margin-top: 40px; margin-bottom: 20px;">
		<div class="my slider">
			<ul class="slides z-depth-4">
				<li><img src="images/bacter.jpg"></li>
			</ul>
		</div>
		<p> 
		<h3 class="my center">Pirmasis pasaulyje alus su probiotikais</h3><br>
		Vos prieš porą dešimtmečių terminas „probiotikai“ buvo žinomas tik ekspertams, o šiandien maisto produktų, kurių sudėtyje yra probiotikų atsiranda vis daugiau ir daugiau. Kas įdomiausia, probiotinėmis bakterijomis, gerinančiomis žarnyno sveikatingumą ir skatinančioms imunitetą, dabar yra praturtinami ne tik pieno produktai. Vienas tokių produktų – alus. Ilgą laiką alus ir probiotikai atrodė nesuderinami dalykai. Natūraliai aluje yra rūgščių kurios slopina probiotinių bakterijų augimą ir galiausiai priverčia jas žūti. Tačiau šis reiškinys vis dėlto atkreipė Nacionalinio Singapūro universiteto mokslininkų dėmesį. Po ilgų bandymų jiems pavyko sukurti alų, kuriame probiotiko Lactobacillus paracasei L26 padermė išlieka gyvybinga. Tiesa, toks alus turi šiek tiek mažesnį alkoholio tūrį. Jis siekia apie 3,5 proc., o didesnio kol kas išgauti nepavyksta. Alaus su probiotikais receptas ir gamybos technologija jau sėkmingai užpatentuoti. Netrukus toks alus turėtų pasirodyti parduotuvių lentynose. Turint omenyje visus probiotikų privalumus ir teigiamai sveikatą veikiančius veiksnius, tikimasi, jog ilgainiui susiformuos visiškai nauja alaus rinka.</p><br><br>

		<!-- Social -->
		<div class="col s10 m7 l4 offset-s1 offset-m4 offset-l5">
			<a href="https://twitter.com/intent/tweet?url=http%3A%2F%2Fwww.cssflow.com%2Fsnippets%2Fshare-buttons&amp;via=CSSFlow&amp;text=CSS3%20snippet%3A%20share%20buttons" class="share-btn" target="_blank" style="margin-right: 5px;">
	      		<span class="share-btn-action share-btn-tweet">Tweet</span>
	      		<span class="share-btn-count">481</span>
		    </a>			
			<a href="https://www.facebook.com/sharer/sharer.php?u=http%3A%2F%2Fwww.cssflow.com%2Fsnippets%2Fshare-buttons" class="share-btn" target="_blank" style="margin-right: 5px;">
	      		<span class="share-btn-action share-btn-like">Like</span>
	      		<span class="share-btn-count">516</span>
		    </a>			
			<a href="https://plus.google.com/share?url=http%3A%2F%2Fwww.cssflow.com%2Fsnippets%2Fshare-buttons" class="share-btn" target="_blank">
		      	<span class="share-btn-action share-btn-plus">+1</span>
		      	<span class="share-btn-count">234</span>
		    </a><br><br><br>
		</div>		
	</div>		

	<!-- News 3 card -->
	<div class="col s12 m10 l4 marg offset-m1">		    
	    <div class="card horizontal z-depth-4">
	      	<div class="card-image">
	        	<img src="images/news3.jpg">
	      	</div>
	      	<div class="card-stacked">
	       		<div class="card-content">
	          	<p>"Craft“ alus keičia lietuvių suvokimą apie alų.</p>
	        	</div>
	        	<div class="card-action">
	          	<a style="color: white;" href="news_3.php?id=3">Skaityti daugiau</a>
	        	</div>
	      	</div>
	    </div>
	</div>	

	<!-- News 1 card -->
    <div class="col s12 m10 l4 marg offset-m1">		    
	    <div class="card horizontal z-depth-4">
	      	<div class="card-image">
	        	<img src="images/news1.jpg">
	      	</div>
	      	<div class="card-stacked">
	       		<div class="card-content">
	          	<p>Alkoholiniai gėrimai tik nuo 20 metų.</p>
	        	</div>
	        	<div class="card-action">
	          	<a style="color: white;" href="news_1.php?id=1">Skaityti daugiau</a>
	        	</div>
	      	</div>
	    </div>
	</div>	
<!-- Vieta komentarams -->
<?php
	// define variables and set to empty values
	$nameErr = $emailErr = $articleidErr = "";
	$name = $email = $comment = $articleid = "";
	if ($_SERVER["REQUEST_METHOD"] == "POST") {

		if (empty($_POST["articleid"])){
			$articleidErr = "Incorrect ID";
		} else {
		  	$articleid = $_POST["articleid"];
			$_GET['id'] = $articleid;
			if( ! is_numeric($articleid) ){
    		die('invalid article id');}
			}    		
		

		if (empty($_POST["firstname"])){
		$nameErr = "Prašome įvesti vardą";
		} else {
			$name = test_input($_POST["firstname"]);
			//check input characters
			if (!preg_match("/^[a-zA-Z ]*$/", $name)) {
				$nameErr = "Tik raidės ir tarpai";
			}
		}

  		if (empty($_POST["email"])) {
  			$emailErr = "Prašome įvesti el. paštą";
  		} else {
  			$email = test_input($_POST["email"]);
  			//check email
  			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
  				$emailErr = "Netinkama el. pašto įvestis";
  			}
  		}		

  	  	$comment = test_input($_POST["textarea"]);

  		// prepare and bind
		$stmt = $conn->prepare("INSERT INTO comments (name, email, comment, articleid) VALUES (?, ?, ?, ?)");
		$stmt->bind_param("sssi", $name, $email, $comment, $articleid);

		// set parameters and execute
		$stmt->execute();
  	}

$sql = "SELECT * FROM comments WHERE articleid =1";
$result = $conn->query($sql);

function test_input($data) {
  	$data = trim($data);
  	$data = stripslashes($data);
  	$data = htmlspecialchars($data);
  	return $data;
}
?>

	<div class="row">
		<div class="col s12 m10 l10 offset-l1 offset-m1 white z-depth-4" >
			<h4>Palikite komentarą</h4>
			<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" style="border: solid 1px;">
				<div class="row">
					<p><span style="color: red">* privalomi laukai.</span></p>
			        <div class="input-field col s6 m6 l6">	
			        	<span style="color: red">*<?php echo $nameErr;?></span><br><br>
			          	<input placeholder="John" id="first_name" name="firstname" type="text" class="validate">			          	
			          	<label for="first_name">Vardas</label>      		          	
			        </div>      
			        <div class="input-field col s6 m6 l6">
			        	<span style="color: red">*<?php echo $emailErr;?></span><br><br>
			          	<input placeholder="john@smith.com" id="email" name="email" type="email" class="validate">
			          	<label for="email">El. paštas</label>			          	
			        </div>
      			</div>
      			<div class="row">
			        <div class="input-field col s10 m8 l6">
			        	<br><br>
			          	<textarea placeholder="Your message" id="textarea1" name="textarea" class="materialize-textarea"></textarea>
          				<label for="textarea1">Komentaras</label>  
          				<input type='hidden' name='articleid' id='articleid' value='<?php echo $_GET["id"]; ?>' />
          				<button class="btn-large waves-effect waves-light" type="submit" name="commentSubmit">Patvirtinti
					    <i class="material-icons right">send</i></button>  
			        </div>
      			</div>
			</form>
			<h4>Paskutiniai komentarai</h4>
<?php 
	if ($result->num_rows > 0):
    // output data of each row
    while($row = $result->fetch_assoc()) :?> 

			<div class="row" style="border: solid 1px;">
				<div class="col s12 m5 l4">
					<p><b>Vardas:</b> <?= $row["name"];?></p>
				</div>
				<div class="col s12 m5 l4">
					<p><b>El. paštas:</b> <?= $row["email"];?></p>
				</div>
				<div class="col s12 m8 l7">
					<p><b>Komentaras:</b> <?= $row["comment"];?></p>					
				</div>
			</div>
       
<?php endwhile;
else:?>
  <?php  echo "Komentarų nėra";
  echo $articleid;
 endif;
$conn->close(); ?>
 	 
		</div>
	</div>	
</div>

<!-- Footer -->
	<footer class="dark_color page-footer wow fadeInUp">
	  	<div class="container">
	    <div class="row">		    
	    	<div class="col s12 m12 l10 offset-l1" style="display: inline;">
		        <h5 class="white-text">Mus galite rasti</h5>
		        <ul class="social-icons icon-circle list-unstyled list-inline">
		        	<li> <a href="https://www.facebook.com/AlausBrolija/"><i class="fa fa-facebook"></i></a></li> 		      		 
		      		<li> <a href="https://foursquare.com/explore?mode=url&near=Vilnius%2C%20Lithuania&nearGeoId=72057594038521052&q=Craft%20beer"><i class="fa fa-foursquare"></i></a></li>   		      		    		 
		      		<li> <a href="https://www.instagram.com/thebottletrade/"><i class="fa fa-instagram"></i></a></li> 		      		       		 
		      		<li> <a href="https://www.pinterest.com/craftbeertime/"><i class="fa fa-pinterest"></i></a></li>    		
		  		</ul>
	      	</div>
	    </div>
	  	</div>
	  	<div class="footer-copyright">
	    <div class="container">
		    © 2017 Lietuvos alus
		    <a class="grey-text text-lighten-4 right" href="#!">Daugiau nuorodų</a>
	    </div>
	  	</div>
	</footer>

</body>
</html>